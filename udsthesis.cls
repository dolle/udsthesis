%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Phd thesis class
% This class is a fork of 'couv.cls' V1.2 23/04/2010
% from Remi Coulon.
% Compiler compatibility: pdflatex/luatex/xelatex
% Bib library compatibility: biblatex biber
%
% Copyright (s) University of Strasbourg
% Author(s) Guillaume Dolle - v.1.0
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{udsthesis}[01/05/2015, V1.0]
\LoadClass[a4paper,12pt,twoside]{book}

% Compatibility with Luatex
\usepackage{ifluatex}
\ifluatex
  \RequirePackage{fontspec}
\else
  \RequirePackage[utf8]{inputenc}
  \RequirePackage[T1]{fontenc}
\fi

% French is the default language, use
\RequirePackage[english,francais]{babel}

% Othe packages
\RequirePackage{fancyhdr}
\RequirePackage{amsfonts,amsmath,amssymb,amsthm}
\RequirePackage{xcolor}
\RequirePackage{tikz}
\usetikzlibrary{calc}
%\RequirePackage{pgfplots,pgfplotstable}
%\RequirePackage{graphicx}
%\RequirePackage{subfigure}

\RequirePackage[colorlinks]{hyperref}
\RequirePackage{ifthen}
%\RequirePackage[acronym]{glossaries}

%\RequirePackage{wallpaper}
\RequirePackage{anysize}
\RequirePackage[absolute]{textpos}
%\RequirePackage{color}

\RequirePackage[backend=biber]{biblatex}

% Institutes variables
\newcommand{\institute}[1]{\def\@institute{#1}}
\newcommand{\umr}[1]{\def\@umr{#1}}
\newcommand{\city}[1]{\def\@city{#1}}
\newcommand{\address}[1]{\def\@address{#1}}
\newcommand{\website}[1]{\def\@website{#1}}
\newcommand{\phone}[1]{\def\@phone{#1}}
\newcommand{\fax}[1]{\def\@fax{#1}}
\newcommand{\email}[1]{\def\@email{#1}}
\newcommand{\issn}[1]{\def\@issn{#1}}
\newcommand{\pubtype}[1]{\def\@pubtype{#1}}
\newcommand{\publiheader}[1]{\def\@publiheader{#1}}
\newcommand{\speciality}[1]{\def\@speciality{#1}}
\newcommand{\dateheader}[1]{\def\@dateheader{#1}}
\newcommand{\jury}[1]{\def\@jury{#1}}
\newcommand{\summary}[1]{\def\@summary{#1}}
\newcommand{\issnorder}[1]{\def\@issnorder{#1}}
\newcommand{\telnum}[1]{\def\@telnum{#1}}

% Variables default values
\institute{INSTITUT DE RECHERCHE MATH\'EMATIQUE AVANC\'EE}
\umr{UMR 7501}
\city{Strasbourg}
\address{Universit\'e de Strasbourg et CNRS \\
 	7 Rue Ren\'e Descartes \\
        67 084 STRASBOURG CEDEX}
\website{www-irma.u-strasbg.fr}
\phone{T\'el. 03 68 85 01 29}
\fax{Fax 03 68 85 03 28}
\email{irma@math.unistra.fr}
\issn{ISSN 0755-3390}
\pubtype{\hspace{0pt}}
\publiheader{\hspace{0pt}}
\speciality{\hspace{0pt}}
\dateheader{\hspace{0pt}}
\jury{Use jury to fill with jury member names}
\summary{Use summary command to fill the summary}
\issnorder{Use issnorder to fill the ISSN number}
\telnum{Use telnum to fill the TEL publication number}

%-------------------------------------------------------------------------------
% CLASS OPTIONS
%-------------------------------------------------------------------------------

% Type of publication.

\DeclareOption{phdmath}
{
  \pubtype{\huge Th\`ese}
  \publiheader{pr\'esent\'ee pour obtenir le grade de docteur de l'Universit\'e de Strasbourg}
  \speciality{Sp\'ecialit\'e MATH\'EMATIQUES}
  \dateheader{Soutenue le \@date\\ devant la commission d'examen}
}

\DeclareOption{phdmathapp}
{
  \pubtype{\huge Th\`ese}
  \publiheader{pr\'esent\'ee pour obtenir le grade de docteur de l'Universit\'e de Strasbourg}
  \speciality{Sp\'ecialit\'e MATH\'EMATIQUES APPLIQU\'EES}
  \dateheader{Soutenue le \@date\\ devant la commission d'examen}
}

\DeclareOption{hdrmath}
{
  \pubtype{\Large Habilitation \`a diriger des recherches}
  \publiheader{Universit\'e de Strasbourg}
  \speciality{Sp\'ecialit\'e MATH\'EMATIQUES}
  \dateheader{Soutenue le \@date\\ devant la commission d'examen}
}

\DeclareOption{hdrmathapp}
{
  \pubtype{\Large Habilitation \`a diriger des recherches}
  \publiheader{Universit\'e de Strasbourg}
  \speciality{Sp\'ecialit\'e MATH\'EMATIQUES APPLIQU\'EES}
  \dateheader{Soutenue le \@date\\ devant la commission d'examen}
}

% Add this option to have to prepublication cover
\DeclareOption{prepub}
{
  \pubtype{\Large Pr\'epublication}
  \publiheader{\hspace{0pt}}
  \speciality{\hspace{0pt}}
  \dateheader{\hspace{0pt}}
  \dateheader{\@date}
  \jury{\hspace{0pt}}
}

\ExecuteOptions{phdmath}

%
\def\logoIRMA{figures/logos/logoIRMA.pdf}
\def\logoUDS{figures/logos/logoUDS.pdf}
\def\logoCNRS{figures/logos/logoCNRS.pdf}
\def\figFormula{figures/formules.pdf}

% definition des parametres de la page de garde (en centimetres)
% definitions generales
\def\margeint{2.2} % marge de interieure
\def\margeext{1}   % mage de exterieure
\def\margehaut{1}  % marge superieure
\def\margebas{1}   % marge inferieure
% defintions specifiques pour la premiere de couverture
\def\hauteurlogo{3}     % hauteur de l'espace reserve aux logos
\def\largeurbande{5.2}	% largeur de la bande bleue foncee
% definitions specifiques pour la quatrieme de couverture
\def\hauteurcadre{12.5}	% hauteur du cadre avec le rappel des references du labo

% reglages du positionnement absolu
\setlength{\TPHorizModule}{10mm}
\setlength{\TPVertModule}{\TPHorizModule}
\textblockorigin{0mm}{0mm}

% definitions de couleurs
\definecolor{bleufonce}{rgb}{0.11,0.38,0.72}

% conversion en longueur pour les besoins latex
\newlength{\margeintlength}
\newlength{\margeextlength}
\newlength{\margehautlength}
\newlength{\margebaslength}
\newlength{\hauteurlogolength}
\newlength{\largeurbandelength}
\newlength{\hauteurcadrelength}

\setlength{\margeintlength}{\margeint cm}
\setlength{\margeextlength}{\margeext cm}
\setlength{\margehautlength}{\margehaut cm}
\setlength{\margebaslength}{\margebas cm}
\setlength{\hauteurlogolength}{\hauteurlogo cm}
\setlength{\largeurbandelength}{\largeurbande cm}
\setlength{\hauteurcadrelength}{\hauteurcadre cm}

% premiere de couverture
% calcul de la largeur de texte restante dans le cadre bleu clair
% on prevoit une marge de 2mm de chaque cote pour le texte situe dans le cadre bleu clair
\newlength{\largeurtextelength}
\setlength{\largeurtextelength}{20.6 cm}
\addtolength{\largeurtextelength}{-\margeintlength}
\addtolength{\largeurtextelength}{-\margeextlength}
\addtolength{\largeurtextelength}{-\largeurbandelength}

% quatrieme de couverture
% calcul de la largeur de texte disponible pour le resume
\newlength{\largeurresumelength}
\setlength{\largeurresumelength}{21 cm}
\addtolength{\largeurresumelength}{-\margeintlength}
\addtolength{\largeurresumelength}{-\margeextlength}

% realisation de la premiere de couverture
% tronc commun : bandeau bleu, IRMA, logos, etc
\newcommand{\backcoverpage}
{
  \begin{tikzpicture}[remember picture,overlay]
        % definition des coordonnees
    \draw (current page.north west) +(\margeint,-\margehaut) coordinate (A);	% coin superieur gauche du cadre bleu clair
    \draw (current page.north east) +(-\margeext -\largeurbande, -\margehaut) coordinate (B);	% coin superieur gauche du cadre bleu fonce
    \draw (current page.south east) +(-\margeext, \margebas + \hauteurlogo) coordinate (C);		% coin inferieur droit des deux cadres bleus
    \draw (current page.north east) +(-\margeext - 0.5*\largeurbande,-\margehaut) coordinate (I);	% point de depart pour les references de l'insitut
    \draw (current page.south east) +(-\margeext - 0.5*\largeurbande, \margebas+\hauteurlogo) coordinate (J);	% point de depart pour l'adresse web de l'institut.

    % dessin du cadre bleu clair
    \fill[color=bleufonce, opacity=0.1] (A) rectangle (C);
    % dessin du cadre bleu foncé
    \fill[color=bleufonce] (B) rectangle (C);
A class option is missing!

    % mise en place des logos
    \draw (current page.south east) +(-\margeext -\largeurbande ,\margebas+ 0.5*\hauteurlogo) node[anchor=center] (UDS) {\includegraphics[height=0.8\hauteurlogolength]{\logoUDS}};
    \draw (current page.south west) +(\margeint ,\margebas+ 0.5*\hauteurlogo) node[anchor=west] (CNRS) {\includegraphics[height=0.8\hauteurlogolength]{\logoCNRS}};
    \draw ($0.5*(CNRS.east)+0.5*(UDS.west)$) node[anchor=center]{\includegraphics[height=0.8\hauteurlogolength]{\logoIRMA}};

    % mise en place des textes de la bande bleue foncee
    \draw (I) +(0,-0.3) node[anchor=north, text=white, text width=0.9\largeurbandelength, text badly centered]{
      \fontfamily{phv}
      \fontsize{16}{23}
      \selectfont
      \@institute
      \\ \vspace*{6mm}
      \umr
      \\ \vspace*{6mm}
      \@city
    };

    % adresse web
    \draw (J) +(0,+0.3) node[anchor=south, text=white, text width=0.9\largeurbandelength, text badly centered]{
      \fontfamily{phv}
      \fontsize{13}{17}
      \selectfont
      \@website
    };
  \end{tikzpicture}
}

% Mise en place du texte principal pour les theses et les HDR
\newcommand{\frontcoverpage}{
  \begin{tikzpicture}[remember picture,overlay]
    \draw (B) +(-0.2,0) node[anchor=north east, text=bleufonce, text width=\largeurtextelength]{
      \fontfamily{phv}%
      \fontsize{12}{18}
      \selectfont%
      \raggedleft%
      \vspace{5cm}
      {\bfseries \@pubtype} \\
      \vspace*{5mm}
      {\large \@publiheader} \\
      {\large \@speciality} \\
      \vspace*{8mm}
      {\large \bfseries \@author} \\
      \vspace{1cm}
      {\large \bfseries \@title} \\
      \vspace{4cm}
      {\large \@dateheader}\\
      \vspace{1cm}
      \@jury\\
    };
  \end{tikzpicture}
}

% Commands
\newcommand\makefrontcover{
  \newpage
  \thispagestyle{empty}
  \backcoverpage
  \frontcoverpage
}

% realisation de la quatrieme de couverture
\newcommand{\makebackcover}{
  \newpage
  \thispagestyle{empty}
  \begin{tikzpicture}[remember picture,overlay]
    % coordonnees des points principaux
    \draw (current page.south west) +(\margeext,\margebas) coordinate (U);	% coin inferieur gauche du cadre
    \draw (current page.south east) +(-\margeint,\margebas+\hauteurcadre) coordinate (V);	% coin superieur gauche du cadre
    \draw (current page.south east) +(-\margeint,\margebas) coordinate (W);	% coin inferieur gauche du cadre
    \draw (current page.north west) +(\margeext,-\margehaut) coordinate (O);	% coin supereur gauche du resume

    % resume de la these
    \draw (O) node[anchor=north west, text width=\largeurresumelength, text justified] {
      \fontfamily{phv}%
      \fontsize{13}{18}
      \selectfont%
      \@summary\\
    };

    % mise en place du fond du cardre avec les formules mathematiques
    \begin{scope}
      \clip (U) rectangle (V);
      \draw (current page.south west) node[anchor=south west]  (fondA) {\includegraphics{\figFormula}};
      \draw (fondA.north west) +(0,-0.2) node[anchor=south west]  (fondB) {\includegraphics{\figFormula}};
      \draw (fondB.north west) +(0,-0.2) node[anchor=south west]  (fondC) {\includegraphics{\figFormula}};
      \draw (fondC.north west) +(0,-0.2) node[anchor=south west]  (fondD) {\includegraphics{\figFormula}};
    \end{scope}
    % dessin du cadre rectangulaire
    \draw[line width=1.5pt, color=bleufonce] (U) rectangle (V);

    % adresse du labo
    \draw (V) +(-0.2,-0.2) node[anchor=north east, text width=\largeurresumelength, color=bleufonce] {
      \fontfamily{phv}%
      \fontsize{13}{18}
      \selectfont%
      \raggedleft
      \@institute\\
      \@umr\\
      \@address\\
      \vspace{5mm}
      \@phone\\
      \@fax\\
      \vspace{5mm}
      \@website\\
      \@email\\
    };

    % adresse tel et ordre dans la serie ISSN
    \draw (W) +(-0.2,0.2) node[anchor=south east, text width=\largeurresumelength, color=bleufonce] {
      \fontfamily{phv}%
      \fontsize{13}{18}
      \selectfont%
      \raggedleft
      IRMA \@issnorder \\
      \@telnum \\
    };

    % numero ISSN
    \draw (U) +(0.2,0.2) node[anchor=south west, text width=\largeurresumelength, color=bleufonce] (ISSN) {
      \fontfamily{phv}%
      \fontsize{13}{18}
      \selectfont%
      \noindent \@issn
    };

    % logos
    \draw (ISSN.north west) +(0,0.8) node[anchor=south west] (IRMA) {\includegraphics[width=3cm]{\logoIRMA}};
    \draw (IRMA.north west) +(0,0.8) node[anchor=south west] (UDS) {\includegraphics[width=3cm]{\logoUDS}};
    \draw (UDS.north west) +(0,0.8) node[anchor=south west] (CNRS) {\includegraphics[width=3cm]{\logoCNRS}};

  \end{tikzpicture}
}

\hypersetup{
    unicode=false,         % non-Latin characters in Acrobat's bookmarks
    pdftoolbar=true,       % show Acrobat's toolbar?
    pdfmenubar=true,       % show Acrobat's menu?
    pdffitwindow=false,    % window fit to page when opened
    pdfborder={0 0 0},     % remove border
    pdfstartview={FitH},   % fits the width of the page to the window
    pdfpagelabels=true,
    pagebackref=true,
    bookmarks=true,
    bookmarksopen=true,
    bookmarksopenlevel=1,
    bookmarksnumbered=true,
    colorlinks=true,   % false: boxed links; true: colored links
    linkcolor=blue!80!black,    % color of internal links
    citecolor=green!50!black,   % color of links to bibliography
    filecolor=magenta, % color of file links
    urlcolor=magenta,  % color of external links
}

\AtEndDocument{
  \pdfinfo{
    /Author (\@author)
    /Title  (\@title)
    /Subject (\@title)
    /Keywords (PDF;LaTeX)
  }
}

\ProcessOptions\relax

%%% Local Variables:
%%% coding: utf-8
%%% mode: latex
%%% End:
% vim:set ft=tex sw=2 tw=80:
